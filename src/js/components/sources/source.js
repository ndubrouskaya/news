export default class Source {
  constructor(name, id, index) {
    this.name = name;
    this.id = id;
    this.index = index;
  }

  create() {
    const element = document.createElement('li');
    element.setAttribute('key', this.index);

    element.innerHTML = `
            <label>
                <input
                    type="radio"
                    name="source"
                    class="source"
                    id=${this.id}
                 >
                 ${this.name}
             </label>
      `;

    return element;
  }
}