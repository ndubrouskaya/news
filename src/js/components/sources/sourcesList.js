import { sourcesClass } from '../../constants';
import Source from './source';

export default class SourcesList {
  constructor(sources) {
    this.sources = sources;
  }

  render() {
    const sourcesContainer = document.querySelector(`.${sourcesClass}`);
    const sourcesList = document.createElement('ul');

    this.sources.forEach((source, index) => {
      const sourceItem = new Source(source.name, source.id, index).create();
      sourcesList.appendChild(sourceItem);
    });
    sourcesContainer.appendChild(sourcesList);
  }
}