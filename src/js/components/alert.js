import * as constants from '../constants';

export default class Alert {
  constructor(message) {
    this.message = message;
  }

  render() {
    const alertBar = document.querySelector(`.${constants.alertClass}`);
    const alertCloseBtn = document.querySelector(`.${constants.alertBtnClass}`);
    const alertText = document.querySelector(`.${constants.alertTextClass}`);

    alertBar.classList.add(constants.showClass);
    alertText.textContent = this.message;
    alertCloseBtn.addEventListener('click', () => {
      alertBar.classList.remove(constants.showClass);
    });
  }
}