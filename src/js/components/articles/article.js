import * as constants from '../../constants';

export default class Article {
  constructor(source = {name: '', id: ''}, title = '', description = '', publishedAt = null, url ='') {
    this.source = source;
    this.title = title;
    this.description = description;
    this.publishedAt = new Date(publishedAt).toLocaleDateString();
    this.url = url;
  }

  create() {
    const article = document.createElement('a');
    article.classList.add(constants.articleClass);
    article.setAttribute('href', `${this.url}`);

    article.innerHTML = `
            <div class="article__container">
                <div class="article__header">${this.title}</div>
                <div class="article__body">${this.description}</div>
                <div class="article__footer">
                    <div class="article__date">${this.publishedAt}</div>
                    <div class="article__source">${this.source.name}</div>
                </div>
             </div>
      `;

    return article;
  }
}
