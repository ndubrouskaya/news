import * as constants from '../../constants';
import Article from './article';

export default class ArticlesList {
  constructor(articles) {
    this.articles = articles;
  }

  render() {
    const articlesList = document.querySelector(`.${constants.articlesClass}`);
    if (articlesList.childElementCount) {
      articlesList.innerHTML = '';
    }
    this.articles.forEach(article => {
      const { title, description, source, url, publishedAt } = article;
      const articlesItem = new Article(source, title, description, publishedAt, url).create();
      articlesList.appendChild(articlesItem);
    });

    const showMoreBtn = document.querySelector(`.${constants.showMoreBtnClass}`);
    showMoreBtn.classList.add(constants.showClass);
  }
}