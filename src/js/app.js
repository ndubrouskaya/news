import * as constants from './constants';
import API from './api';
import Alert from './components/alert';
import ArticlesList from './components/articles/articleList';
import SourcesList from './components/sources/sourcesList';

import '../styles/index.scss';

export default class App {
  constructor() {
    this.articles = [];
    this.page = 0;

    return this.init();
  }

  showAlert({message} = data) {
    new Alert(message).render();
  }

  getSources({sources} = data) {
    new SourcesList(sources).render();
  };

  getArticles({articles} = data) {
    this.articles = [...this.articles, ...articles];

    new ArticlesList(this.articles).render();
  }

  getNews(event) {
    if(event.target.className === constants.showBtnClass) {
      this.page = 0;
      this.articles = [];
    }

    this.page++;

    const sourcesArray = Array.from(document.querySelectorAll(`.${constants.sourceClass}`));
    const checkedSource = sourcesArray.find(item => item.checked);

    if(checkedSource) {
      API.get(
        `${constants.BASE_URL}${constants.EVERYTHING}?sources=${checkedSource.id}&page=${this.page}&apiKey=${constants.API_KEY}`,
        this.getArticles.bind(this),
        this.showAlert
      );
    } else {
      this.showAlert({message: 'There are no checked news source:('});
    }
  }

  init() {
    const showBtn = document.querySelector(`.${constants.showBtnClass}`);
    showBtn.addEventListener('click', this.getNews.bind(this));

    const showMoreBtn = document.querySelector(`.${constants.showMoreBtnClass}`);
    showMoreBtn.addEventListener('click', this.getNews.bind(this));

    API.get(
      `${constants.BASE_URL}${constants.SOURCES}?apiKey=${constants.API_KEY}`,
      this.getSources.bind(this),
      this.showAlert
    );
  }
}
