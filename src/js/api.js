export default class API {
  handleErrors(response) {
    if (!response.ok) {
      throw Error(response);
    }
    return response;
  }

  static get(url, resolve, reject) {
    fetch(url)
      .then(this.handleErrors)
      .then(response => response.json())
      .then(json => resolve(json))
      .catch(error => reject(error));
  }
}