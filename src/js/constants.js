export const BASE_URL = 'https://newsapi.org';
export const SOURCES = '/v2/sources';
export const EVERYTHING = '/v2/everything';
export const API_KEY = '55748311de9b44e8b2929a49db94ac6e';

// constants for html classes
export const showBtnClass = 'show-btn';
export const showMoreBtnClass = 'show-more-btn';
export const showClass = 'show';

export const alertClass = 'alert';
export const alertBtnClass = 'alert__btn';
export const alertTextClass = 'alert__text';

export const sourcesClass = 'sources';
export const sourceClass = 'source';

export const articlesClass = 'articles';
export const articleClass = 'article';
