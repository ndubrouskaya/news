const { join } = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

const baseConfig = require('./webpack.config.base');

module.exports = merge(baseConfig, {
  entry: [
    'babel-polyfill',
    join(__dirname, 'src/js/index.js')
  ],
  plugins: [
    new CleanWebpackPlugin('dist'),
    new UglifyJSPlugin(),
    new OptimizeCssAssetsPlugin(),
  ],
});
